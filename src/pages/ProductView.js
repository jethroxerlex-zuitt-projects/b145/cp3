import { useState, useEffect, useContext } from 'react'
import { Row, Col, Card, Button, Container } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function ProductView() {

	const { user } = useContext(UserContext)

	const history = useNavigate()

	const { productId } = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const purchase = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/add-to-cart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if (data === true) {
					Swal.fire({
						title: "Enrolled Successfully",
						icon: "success",
						text: "Thank you for enrolling!"
					})

					history("/products")
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}
			})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
			})
	}, [productId])

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description: </Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price: </Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							{user.id !== null ?
								<Button variant="outline-dark" onClick={() => purchase(productId)}>Purchase</Button>
								:

								<Link className="btn outline-dark btn-block" to="/login">Login to Purchase</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)

}